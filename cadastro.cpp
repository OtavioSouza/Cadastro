#include "cadastro.h"


string Pessoa::getNome(){
	return nome;
}
void Pessoa::setNome(string name){
	this -> nome = name;
}

string Pessoa::getSobrenome(){
	return sobrenome;
}

void Pessoa::setSobrenome(string sobrenome){
	this -> sobrenome = sobrenome;
}

string Pessoa::getCPF(){
	return cpf;
}

void Pessoa::setCPF(string cpf){
	this -> cpf = cpf;
}

int Pessoa::getregistro(){
	return registro;
}

void Pessoa::setregistro(int registro){
	this -> registro = registro;
}

int Pessoa::getIdPessoa(){
	return idPessoa;
}

void Pessoa::setIdPessoa(int idPessoa){
	this -> idPessoa = idPessoa;
}


//********************************************************************************************************

void disciplina::setProfessor(string Professor){
    this -> Professor = Professor;
}

string disciplina::getProfessor(){
    return Professor;
}

int disciplina::getCoddisciplina(){
	return coddisciplina;
}

void disciplina::setCoddisciplina(int coddisciplina){
	this -> coddisciplina = coddisciplina;
}

string disciplina::getNomedisciplina(){
	return nomedisciplina;
}

void disciplina::setNomedisciplina(string nomedisciplina){
	this -> nomedisciplina = nomedisciplina;
}


//*******************************************************************************************************


string linf::getProposito(){
	return proposito;
}

void linf::setProposito(string proposito){
	this -> proposito = proposito;
}

void linf::setHoraAula(int horaAula){ 
	this -> horaAula = horaAula;
}

int  linf::getHoraAula(){ 
	return horaAula;
}

void linf::setAteQualDia(string AteQualDia){ 
	this -> AteQualDia = AteQualDia;
}

string linf::getAteQualDia(){ 
	return AteQualDia;
}

int linf::getSala(){
	return sala;
}

void linf::setSala(int sala){
	this -> sala = sala;
}

void linf::setdiasAula(int diasAula){
    this -> diasAula = diasAula;
}

int linf::getdiasAula(){
    return diasAula;
}


//********************************************************************************************
void BancoDados::AddUser(){
	string nome , sobrenome, cpf ;
	int registro , idPessoa; 
	Pessoa p1;

	system("clear");
	cout << endl;
	cout << endl << "Informe o nome: ";
	cin >> nome;
	p1.setNome(nome);
	
	cout << endl << "Informe o sobrenome: ";
	cin >> sobrenome;
	p1.setSobrenome(sobrenome);

	cout << endl << "Informe o CPF: ";
	cin >> cpf;
	p1.setCPF(cpf);

	cout << endl;
	cout << "*********************************************" <<endl;
	cout << "Escolha em qual opcao vc esta classificado: *" <<endl;
	cout << "1 - aluno                                   *" <<endl;
	cout << "2 - funcionario da limpeza                  *" <<endl;
	cout << "3 - funcionario de segurança                *" <<endl;
	cout << "4 - funcionario de suporte                  *" <<endl;
	cout << "5 - pessoas externas                        *" <<endl;
	cout << "6 - Deseja sair                             *" <<endl;
	cout << "*********************************************" <<endl;
	cout << endl <<  "Opcao: ";
	cin >> idPessoa;

    switch(idPessoa){
        case 1:p1.setIdPessoa(1); break;
        case 2:p1.setIdPessoa(2); break;
        case 3:p1.setIdPessoa(3); break;
        case 4:p1.setIdPessoa(4); break;
        case 5:p1.setIdPessoa(5); break;
        case 6: break;
        default: cout << "opcao invalida" << endl;
    }

	if(idPessoa == 1){
		cout << endl << "informe a matricula : ";
		cin >> registro;
		p1.setregistro(registro);
	} 
	else{
		p1.setregistro(0);
	}

	vet_pessoas.push_back(p1);

    cout << "Aperte <enter> para voltar ao Menu principal: " << endl;
    getchar();
    getchar();

}		

void BancoDados::VerificaPessoas(){
	system("clear");
    
        for(int i = 0 ; i < (int)vet_pessoas.size() ; i++){
            cout << endl << "************CADASTRO**************" << endl;
            cout << endl << "**********************************";
            cout << endl << "Nome : " << vet_pessoas[i].getNome() <<" "<< vet_pessoas[i].getSobrenome();
            cout << endl << "Matricula: " << vet_pessoas[i].getregistro();
        
            if(vet_pessoas[i].getIdPessoa() == 1)
                cout << endl << "Aluno";
            else if(vet_pessoas[i].getIdPessoa() == 2)
                cout << endl << "funcionario da limpeza";
            else if(vet_pessoas[i].getIdPessoa() == 3)
                cout << endl << "funcionario de segurança";
            else if(vet_pessoas[i].getIdPessoa() == 4)
                cout << endl << "funcionario de suporte";
            else if(vet_pessoas[i].getIdPessoa() == 5)
                cout << endl << "pessoas externas";
            cout << endl << "**********************************";
            cout << endl;
        }
	
	cout << endl << "Aperte <enter> para voltar ao Menu principal: "; 
	getchar();
	getchar();

}

void BancoDados::AddDisciplinas(){
	disciplina d1;

	string nome , Professor , Sobrenome;
	int codigo;
	
	system("clear");
	cout<<endl<<"Digite o codigo da disciplina: ";
	cin >> codigo;
	cout<< endl<<"Digite nome da disciplina : ";
	cin >> nome;
    cout<< endl<<"Digite nome do professor da disciplina : ";
    cin >> Professor;

	d1.setCoddisciplina(codigo);
	d1.setNomedisciplina(nome);
    d1.setProfessor(Professor);

	vet_Disciplinas.push_back(d1);

    cout << "Aperte <enter> para voltar ao Menu principal: " << endl;
    getchar();
    getchar();

}

void BancoDados::VerificaDisciplinas(){
	system("clear");
	for(int i = 0 ; i < (int)vet_Disciplinas.size() ; i++){
		cout << endl << "**********************************" << endl;
		cout << "Nome da disciplina: " << vet_Disciplinas[i].getNomedisciplina() << endl;
		cout << "Codigo: " << vet_Disciplinas[i].getCoddisciplina() << endl;
        cout << "Professor: "<< vet_Disciplinas[i].getProfessor() << endl;
		cout << "**********************************" << endl;
        cout<<endl;
	}
	cout << endl << "Aperte <enter> para voltar ao Menu principal: "; 
	getchar();
	getchar();
}

void BancoDados::ResetDisciplinas(){
	int codigo;
    bool achou = false;
    system("clear");
	cout<<endl<<"Digite o codigo da disciplina: ";
	cin >> codigo;

	for(int i = 0 ; (int)vet_Disciplinas.size() && !achou ; i++){
		if(vet_Disciplinas[i].getCoddisciplina() == codigo){
            vet_Disciplinas.erase(vet_Disciplinas.begin()+i);
            cout << "Nome da disciplina: " << vet_Disciplinas[i].getNomedisciplina() << endl;
            cout << "Professor: "<< vet_Disciplinas[i].getProfessor() << endl;
            cout << endl << "Disciplina excluida com sucesso" << endl;
            achou = true;
		}
	}

    if (!achou) {
        cout << endl << "Disciplina inexistente"<< endl;
    }

    cout << endl << "Aperte <enter> para voltar ao Menu principal: ";
    getchar();
    getchar();
}

void BancoDados::AltDisciplinas(){
	int codigo , novoCod ,flag = 0;
	 string disciplina , Professor;

	system("clear");
	cout << endl << "Informe o codigo da discilina que deseja alterar: ";
	cin >> codigo;

	for(int i = 0 ; i < (int)vet_Disciplinas.size() ; i++){
		if(vet_Disciplinas[i].getCoddisciplina() == codigo){
			cout << endl<< "informe o codigo da nova disciplina :";
			cin >> novoCod;
			cout << endl<< "informe o nome da nova disciplina :";
			cin >> disciplina;
            cout << endl<< "informe o professor da nova disciplina :";
            cin >> Professor;
			vet_Disciplinas[i].setCoddisciplina(novoCod);
			vet_Disciplinas[i].setNomedisciplina(disciplina);
            vet_Disciplinas[i].setProfessor(Professor);
            flag = 1;
		}
	}

    if(flag == 1)
        cout << endl << "Alteracao realizada com sucesso" << endl;
    else if(flag == 0)
        cout << endl << "Disciplina inexistente"<< endl;

    cout << "Aperte <enter> para voltar ao Menu principal : " << endl;
    getchar();
    getchar();
}

//*******************************************
void BancoDados::AgendaLinf(){

    system("clear");
	
    for(int i = 0; i < (int)vetReservas.size() ; i++){
        if(vetReservas[i].getIdPessoa() == 1 || vetReservas[i].getIdPessoa() == 2 || vetReservas[i].getIdPessoa() == 3 || vetReservas[i].getIdPessoa() == 4 || vetReservas[i].getIdPessoa() == 5){
    		cout << endl << "**********************************" << endl;
    		cout << "Nome : " << vetReservas[i].getNome() <<" "<< vetReservas[i].getSobrenome() << endl;
    		cout << "Matricula: " << vetReservas[i].getregistro() << endl; 
    		
            if(vetReservas[i].getIdPessoa() == 1)
                cout << "Aluno" <<endl;
            else if(vetReservas[i].getIdPessoa() == 2)
                cout << "Funcionario de limpeza" <<endl;   
            else if(vetReservas[i].getIdPessoa() == 3)
                cout << "Funcionario de Seguranca" <<endl;
            else if(vetReservas[i].getIdPessoa() == 4)
                cout << "Funcionario de suporte" <<endl;
            else if(vetReservas[i].getIdPessoa() == 5)
                cout << "Externos" <<endl;
    		
    		cout << "Proposito da reserva : " << vetReservas[i].getProposito() << endl;
    		cout << "Sala : " << vetReservas[i].getSala() << endl;

            if(vetReservas[i].getHoraAula() == 1)
                cout << "Horario da reserva : 8h as 10h" << endl;
            else if(vetReservas[i].getHoraAula() == 2)
                cout << "Horario da reserva : 10h as 12h" << endl;
            else if(vetReservas[i].getHoraAula() == 3)
                cout << "Horario da reserva : 12h as 14h" << endl;
            else if(vetReservas[i].getHoraAula() == 4)
                cout << "Horario da reserva : 14h as 16h" << endl;
            else if(vetReservas[i].getHoraAula() == 5)
                cout << "Horario da reserva : 16h as 18h" << endl;
            else if(vetReservas[i].getHoraAula() == 6)
                cout << "Horario da reserva : 19h as 21h" << endl;
            else if(vetReservas[i].getHoraAula() == 7)
                cout << "Horario da reserva : 21h as 23h" << endl;

            cout << "Reserva adquirida ate o dia : " << vetReservas[i].getAteQualDia() << endl; 
            
            if(vetReservas[i].getdiasAula() == 1)
                cout << "Dias da reserva : Domingo"<< endl;
            else if(vetReservas[i].getdiasAula() == 2)
                cout << "Dias da reserva : Segunda"<< endl;
            else if(vetReservas[i].getdiasAula() == 3)
                cout << "Dias da reserva : Terca"<< endl;
            else if(vetReservas[i].getdiasAula() == 4)
                cout << "Dias da reserva : Quarta"<< endl; 
            else if(vetReservas[i].getdiasAula() == 5)
                cout << "Dias da reserva : Quinta"<< endl;
            else if(vetReservas[i].getdiasAula() == 6)
                cout << "Dias da reserva : Sexta"<< endl;
            else if(vetReservas[i].getdiasAula() == 7)
                cout << "Dias da reserva : Sabado"<< endl;
            cout << endl << "**********************************" << endl;
        }
        else{
            cout << endl << "**********************************" << endl;
            cout << "Professor : " << vetReservas[i].getProfessor() << endl;
            cout << "Nome da disciplina: " << vetReservas[i].getNomedisciplina() << endl;
            cout << "Codigo da disciplina: " << vetReservas[i].getCoddisciplina() << endl;        
            cout << "Proposito da reserva : " << vetReservas[i].getProposito() << endl;
            cout << "Sala : " << vetReservas[i].getSala() << endl; 

            if(vetReservas[i].getHoraAula() == 1)
                cout << "Horario da reserva : 8h as 10h" << endl;
            else if(vetReservas[i].getHoraAula() == 2)
                cout << "Horario da reserva : 10h as 12h" << endl;
            else if(vetReservas[i].getHoraAula() == 3)
                cout << "Horario da reserva : 12h as 14h" << endl;
            else if(vetReservas[i].getHoraAula() == 4)
                cout << "Horario da reserva : 14h as 16h" << endl;
            else if(vetReservas[i].getHoraAula() == 5)
                cout << "Horario da reserva : 16h as 18h" << endl;
            else if(vetReservas[i].getHoraAula() == 6)
                cout << "Horario da reserva : 19h as 21h" << endl;
            else if(vetReservas[i].getHoraAula() == 7)
                cout << "Horario da reserva : 21h as 23h" << endl;

            cout << "Reserva adquirida ate o dia : " << vetReservas[i].getAteQualDia() << endl;
            
            if(vetReservas[i].getdiasAula() == 1)
                cout << "Dias da reserva : Domingo"<< endl;
            else if(vetReservas[i].getdiasAula() == 2)
                cout << "Dias da reserva : Segunda"<< endl;
            else if(vetReservas[i].getdiasAula() == 3)
                cout << "Dias da reserva : Terca"<< endl;
            else if(vetReservas[i].getdiasAula() == 4)
                cout << "Dias da reserva : Quarta"<< endl; 
            else if(vetReservas[i].getdiasAula() == 5)
                cout << "Dias da reserva : Quinta"<< endl;
            else if(vetReservas[i].getdiasAula() == 6)
                cout << "Dias da reserva : Sexta"<< endl;
            else if(vetReservas[i].getdiasAula() == 7)
                cout << "Dias da reserva : Sabado"<< endl;
            cout << endl << "**********************************" << endl;
        }
        
	}
	
	cout << "Aperte <enter> para voltar ao Menu principal : " << endl;
    getchar();
    getchar();
}

void BancoDados::AddReservaProf(){
	int recorrente , sala , horario , dia ;
	std::string nome , proposito, cpf, AteQualDia;
    linf l1;

    system("clear");
    cout << endl << "Informe o nome do Professor: ";
    cin >> nome;
    
    for(int i = 0 ; i < (int)vet_Disciplinas.size() ; i++){
        if(vet_Disciplinas[i].getProfessor() == nome){
            cout << endl << "Professor : " << vet_Disciplinas[i].getProfessor();
            l1.setProfessor(vet_Disciplinas[i].getProfessor());
            cout << endl << "Disciplina : " << vet_Disciplinas[i].getNomedisciplina();
            l1.setNomedisciplina(vet_Disciplinas[i].getNomedisciplina());
            cout << endl << "Codigo : " << vet_Disciplinas[i].getCoddisciplina();
            l1.setCoddisciplina(vet_Disciplinas[i].getCoddisciplina());

            system("clear");
            cout << endl << "informe o proposito da reserva: ";
            cin >> proposito;
            l1.setProposito(proposito);
            
            system("clear");
            cout << endl << "***************************************";
            cout << endl << "Informe o dia da reserva: ";
            cout << endl << "1 - domingo";
            cout << endl << "2 - segunda";
            cout << endl << "3 - terca";
            cout << endl << "4 - quarta";
            cout << endl << "5 - quinta";
            cout << endl << "6 - sexta";
            cout << endl << "7 - sabado";
            cout << endl << "***************************************";
            cout << endl << "Opcao : ";
            cin >> dia;
            
            switch(dia){
                case 1: l1.setdiasAula(1); break;
                case 2: l1.setdiasAula(2); break;
                case 3: l1.setdiasAula(3); break;
                case 4: l1.setdiasAula(4); break;
                case 5: l1.setdiasAula(5); break;
                case 6: l1.setdiasAula(6); break;
                case 7: l1.setdiasAula(7); break;
                default : cout << endl <<"opcao invalida "; break;  
            }
            
            system("clear");
            cout << endl << "***************************************";
            cout << endl <<"Selecione o horario do servico : ";
            cout << endl <<"Opcoes de horario : ";
            cout << endl <<"1- 8h as 10h";
            cout << endl <<"2- 10h as 12h";
            cout << endl <<"3- 12h as 14h";
            cout << endl <<"4- 14h as 16h";
            cout << endl <<"5- 16h as 18h";
            cout << endl <<"6- 19h as 21h";
            cout << endl <<"7- 21h as 23h";
            cout << endl << "***************************************";
            cout << endl << "Opcao : ";
            cin >> horario;
        
            switch(horario){
                case 1: l1.setHoraAula(1); break;
                case 2: l1.setHoraAula(2); break;
                case 3: l1.setHoraAula(3); break;
                case 4: l1.setHoraAula(4); break;
                case 5: l1.setHoraAula(5); break;
                case 6: l1.setHoraAula(6); break;
                case 7: l1.setHoraAula(7); break;
                default : cout << endl <<"opcao invalida "; break;  
            }
            
            system("clear");
            cout << endl <<"Selecione o numero da sala do servico";
            cout << endl <<"Salas de 1 a 5 ";
            cout << endl <<"Sala : ";
            cin >> sala;
            
            switch(sala){
                case 1: l1.setSala(1); break;
                case 2: l1.setSala(2); break;
                case 3: l1.setSala(3); break;
                case 4: l1.setSala(4); break;
                case 5: l1.setSala(5); break;
                default : cout << endl <<"opcao invalida "; break;  
            }
            
            for(int k = 0 ; k < (int)vetReservas.size() ; k++){ 
                if(vetReservas[k].getSala() == sala && vetReservas[k].getHoraAula() == horario && vetReservas[k].getdiasAula() == dia){
                    cout << "Desculpa sala , horario e dia ocupado no momento" << endl;l1.setNome(vet_pessoas[i].getNome());
                }
            }

            system("clear");
            cout << endl << "Aulas recorrentes (sim - 1 , nao - 0): ";
            cin >> recorrente;
            if(recorrente == 1){
                cout << endl << "Ate qual dia havera aula (ex : 15/02): ";
                cin >> AteQualDia;
                l1.setAteQualDia(AteQualDia);
            }
            else if(recorrente == 0)
                l1.setAteQualDia("Sem data");

            cout << "Reserva realizada com sucesso" << endl;      
        }
            
        else
            cout << endl << "Voce nao pode realizar uma reserva !";
        
        vetReservas.push_back(l1);
    }

    cout << "Aperte <enter> para voltar ao Menu principal : " << endl;
    getchar();
    getchar(); 
}

void BancoDados::AddReserva(){
    int recorrente , sala , horario , dia ;
    string nome , proposito, cpf, AteQualDia;
    linf l1;
    
    system("clear");
    cout << endl << "Informe o nome : ";
    cin >> nome;
    l1.setNome(nome);
    
    for(int i = 0 ; i < (int)vet_pessoas.size() ; i++){
        if((vet_pessoas[i].getNome() == nome ) && (vet_pessoas[i].getIdPessoa() == 1)){
            cout << endl << "Matricula : " << vet_pessoas[i].getregistro();
            l1.setregistro(vet_pessoas[i].getregistro());
            l1.setSobrenome(vet_pessoas[i].getSobrenome());
            l1.setIdPessoa(vet_pessoas[i].getIdPessoa()); 

            system("clear");
            cout << endl << "informe o proposito da reserva: ";
            cin >> proposito;
            l1.setProposito(proposito);
            
            system("clear");
            cout << endl << "***************************************";
            cout << endl << "Informe o dia da reserva: ";
            cout << endl << "1 - domingo";
            cout << endl << "2 - segunda";
            cout << endl << "3 - terca";
            cout << endl << "4 - quarta";
            cout << endl << "5 - quinta";
            cout << endl << "6 - sexta";
            cout << endl << "7 - sabado";
            cout << endl << "***************************************";
            cout << endl << "Opcao : ";
            cin >> dia;
            
            switch(dia){
                case 1: l1.setdiasAula(1); break;
                case 2: l1.setdiasAula(2); break;
                case 3: l1.setdiasAula(3); break;
                case 4: l1.setdiasAula(4); break;
                case 5: l1.setdiasAula(5); break;
                case 6: l1.setdiasAula(6); break;
                case 7: l1.setdiasAula(7); break;
                default : cout << endl <<"opcao invalida "; break;  
            }
            
            system("clear");
            cout << endl << "***************************************";
            cout << endl <<"Selecione o horario do servico : ";
            cout << endl <<"Opcoes de horario : ";
            cout << endl <<"1- 8h as 10h";
            cout << endl <<"2- 10h as 12h";
            cout << endl <<"3- 12h as 14h";
            cout << endl <<"4- 14h as 16h";
            cout << endl <<"5- 16h as 18h";
            cout << endl <<"6- 19h as 21h";
            cout << endl <<"7- 21h as 23h";
            cout << endl << "***************************************";
            cout << endl << "Opcao : ";
            cin >> horario;
        
            switch(horario){
                case 1: l1.setHoraAula(1); break;
                case 2: l1.setHoraAula(2); break;
                case 3: l1.setHoraAula(3); break;
                case 4: l1.setHoraAula(4); break;
                case 5: l1.setHoraAula(5); break;
                case 6: l1.setHoraAula(6); break;
                case 7: l1.setHoraAula(7); break;
                default : cout << endl <<"opcao invalida "; break;  
            }
            
            system("clear");
            cout << endl <<"Selecione o numero da sala do servico";
            cout << endl <<"Salas de 1 a 5 ";
            cout << endl <<"Sala : ";
            cin >> sala;
            
            switch(sala){
                case 1: l1.setSala(1); break;
                case 2: l1.setSala(2); break;
                case 3: l1.setSala(3); break;
                case 4: l1.setSala(4); break;
                case 5: l1.setSala(5); break;
                default : cout << endl <<"opcao invalida "; break;  
            }
            
            for(int k = 0 ; k < (int)vetReservas.size() ; k++){ 
                if(vetReservas[k].getSala() == sala && vetReservas[k].getHoraAula() == horario && vetReservas[k].getdiasAula() == dia){
                    cout << "Desculpa sala , horario e dia ocupado no momento" << endl;l1.setNome(vet_pessoas[i].getNome());
                }
            }    
        }
        
        else if(vet_pessoas[i].getNome() == nome){
            
            cout << endl << "Informe o proposito da reserva: ";
            cin >> proposito;
            l1.setProposito(proposito);
            l1.setNome(vet_pessoas[i].getNome());
            l1.setSobrenome(vet_pessoas[i].getSobrenome());
            l1.setregistro(0);
            l1.setIdPessoa(vet_pessoas[i].getIdPessoa());
            
            system("clear");
            cout << endl << "***************************************";
            cout << endl << "Informe o dia da reserva: ";
            cout << endl << "1 - domingo";
            cout << endl << "2 - segunda";
            cout << endl << "3 - terca";
            cout << endl << "4 - quarta";
            cout << endl << "5 - quinta";
            cout << endl << "6 - sexta";
            cout << endl << "7 - sabado";
            cout << endl << "***************************************";
            cout << endl << "Opcao : ";
    
            cin >> dia;
            
            switch(dia){
                case 1: l1.setdiasAula(1); break;
                case 2: l1.setdiasAula(2); break;
                case 3: l1.setdiasAula(3); break;
                case 4: l1.setdiasAula(4); break;
                case 5: l1.setdiasAula(5); break;
                case 6: l1.setdiasAula(6); break;
                case 7: l1.setdiasAula(7); break;
                default : cout << endl <<"opcao invalida "; break;  
            }

            system("clear");
            cout << endl <<"Selecione o numero da sala da reserva";
            cout << endl <<"Salas de 1 a 5 ";
            cout << endl <<"Sala : ";
            cin >> sala;
            
            switch(sala){
                case 1: l1.setSala(1); break;
                case 2: l1.setSala(2); break;
                case 3: l1.setSala(3); break;
                case 4: l1.setSala(4); break;
                case 5: l1.setSala(5); break;
                default : cout << endl <<"opcao invalida "; break;  
            }

            system("clear");
            cout << endl << "***************************************";
            cout << endl <<"Selecione o horario da reserva : ";
            cout << endl <<"Opcoes de horario : ";
            cout << endl <<"1- 8h as 10h";
            cout << endl <<"2- 10h as 12h";
            cout << endl <<"3- 12h as 14h";
            cout << endl <<"4- 14h as 16h";
            cout << endl <<"5- 16h as 18h";
            cout << endl <<"6- 19h as 221h";
            cout << endl <<"7- 21h as 23h";
            cout << endl << "***************************************";
            cout << endl << "Opcao : ";
            cin >> horario;
            
            switch(horario){
                case 1 :  l1.setHoraAula(1); break;
                case 2: l1.setHoraAula(2); break;
                case 3: l1.setHoraAula(3); break;
                case 4: l1.setHoraAula(4); break;
                case 5: l1.setHoraAula(5); break;
                case 6: l1.setHoraAula(6); break;
                case 7: l1.setHoraAula(7); break;
                default : cout << endl <<"opcao invalida "; break;  
            }
            
            for(int y = 0 ; y < (int)vetReservas.size() ; y++){ 
                if(vetReservas[y].getSala() == sala && vetReservas[y].getHoraAula() == horario && vetReservas[y].getdiasAula() == dia){
                    cout << "Desculpa sala , horario e dia ocupados no momento" << endl;
                }
            }

            system("clear");
            cout << endl << "Reserva recorrente (sim - 1 , nao - 0): ";
            cin >> recorrente;
            
            if(recorrente == 1){    
                cout << endl << "Informe ate que dia a reserva permanecera (ex : 15/02): ";
                cin >> AteQualDia;
                l1.setAteQualDia(AteQualDia);
            }
            else if(recorrente == 0)
                l1.setAteQualDia("Sem data");

        }

        else if (vet_pessoas[i].getNome() != nome){
            cout << endl << "Voce nao pode realizar uma reserva";
            cout << endl << "aperte enter para sair ";
            getchar();
        }
        vetReservas.push_back(l1);
    }

    cout << "Aperte <enter> para voltar ao Menu principal : " << endl;
    getchar();
    getchar();         
}

void BancoDados::ResetReservas(){
    
    string nome;
    bool achou = false;
    system("clear");
    
    cout << endl << "Informe o nome : ";
    cin >> nome;
    
    for(int i = 0 ; i < (int)vetReservas.size() && !achou ; i++){
        if(vetReservas[i].getNome() == nome){
            vetReservas.erase(vetReservas.begin()+i);
            cout << "Reserva excluida com sucesso" << endl;
            achou = true;
        }
    }

    if (!achou) {
        cout << endl << "Reserva inexistente"<< endl;
    }

    cout << "Aperte <enter> para voltar ao Menu principal : " << endl;
    getchar();
    getchar();
}


