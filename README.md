Laboratório 2 de Técnicas de programação 1
==========================================

Instituição: Universidade de Brasília;<br>
Aluno: Otávio Souza de Oliveira;<br>
Curso: Ciência da Computação;<br>
Professor: Teófilo de Campos - Turma A.<br>

Introdução
----------
  Relatório referente ao segundo laboratório de Técnicas de programação 1, orientado pelo professor Teófilo de Campos<br>

Objetivo:
---------
  Se familiarizar com a programação orientada a objeto, praticar conceitos relacionados a classes, atributos, métodos, abstração, escapsulamento, herança e polimorfismo; e criar funções que auxiliam na aprendizagem da visão computacional, princípios de "treino de máquina" e uso da biblioteca OpenCV.<br>
  A área de pesquisa relacionada a computação vem quebrando cada vez mais barreiras relacionadas a segurança, não só segurança de dados, mas também segurança física. Pesquisas relacionadas a biometria têm ganhado força.<br>
  Nesse trabalho, foi explorado o uso de biometria facial, onde um banco de dados possui diversas imagens usuários, e com o uso de funções, uso de hardware de camera e drivers, consegue comparar a imagem recebida com essas do banco e descobrir quem é a pessoa em questão.<br>
  É claro que nem sempre o computador acerta, e é nesse momento que entra a ideia de "treino de máquina", onde você enriquece o banco de dados afim de conseguir uma melhor e acertiva combinação da imagem de entrada com as imagens do banco de dados.<br>

2. Requisitos
-------------

2.1 Pacotes:
------------- 
    2.1.1 build-essential<br>
    2.1.2 cmake<br>
    2.1.3 git<br>
    2.1.4 libgtk2.0-dev<br>
    2.1.5 pkg-config<br>
    2.1.6 libavcodec-dev<br>
    2.1.7 libavformat-dev<br>
    2.1.8 libswscale-dev<br>

2.2 Bibliotecas:
----------------
opencv:
-------
  Biblioteca referente as funções de visão computacional, no qual possui as funções que utiliza a camêra do pc, executa uma imagem, trata ela de modo com que é comparado com as imagens do banco de dados, assim sabendo se a pessoa está ou não cadastrada. Caso acerte, a pessoa tem acesso. Caso erre, há duas alternativas, ou a pessoa não está cadastrada ou o programa não a encontrou. Caso seja problema de cadastro, é decidido se a pessoa deve ou não se cadastrar. Caso seja por não ter sido encontrada, a pessoa tira mais algumas fotos para treinar o computador para pesquisas futuras;<br>

iostream:
--------- 
  Biblioteca que auxilia nos prints;<br>

Cadastro:
---------- 
  Biblioteca própria, onde possui as funções que trabalham com o banco de dados e tratamento dos dados entrados pelo usuário durante a execução. É responsável por cuidar de classes e objetos relacionados a matéria, pessoas e reservas do laboratório LINF.<br>

2.3 Compiladores:
-----------------
g++ (GCC) 2015 Free Software Foundation, Inc.<br>

2.4 Versão C++:
---------------
5.3.0<br>

2.5 Instalação dos pacotes/bibliotecas :
-----------------------------------------
Foram utilizadas as seguintes linhas para instalação<br>
<blockquote>
  <pre><code><p>[compiler] sudo apt-get install build-essential<br>
[required] sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev<br>
[optional] sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev </p></code></pre>
</blockquote>


3. Cmake
--------
  Cmake é uma ferramenta que auxilia na compilação de arquivos e projetos, gerenciando de forma que sempre é compilado o que foi atualizado. É um suporte ao makefile, uma vez que ele gera arquivos makefiles a partir do seu projeto atual, agilizando a compilação multiplataforma.<br>
O processo de compilação usando o cmake , é definido nos seguintes passos :<br><br>
Primeiro Deve-se criar um arquivo texto com o nome CMakeLists no diretório desejado. Após ter o arquivo criado , dentro dele você deverá listar alguns comandos para a execução do código , segue alguns desses comandos:<br><br>

<em>cmake_minimum_required(VERSION 2.8):</em> Comando usado para setar a verssão minina do cmake , para a utilização neste projeto criado.<br><br>
<em>project(test):</em> Este comando é o nome da linha aplicação , após o processo de compilação.<br><br>
<em>add_executable(${PROJECT_NAME} cadastro.h cadastro.cpp test-cadastro.cpp):</strong> Comando onde efetivamente acontece o processo de compilação. Lembrando que esses comando devem ser salvos no CmakeLists.txt , após deve-se criar um diretório para ocorrer a compilação dos arquivos , e utilizar os comandos cmake e make para compilar e executar o arquivo , segue abaixo a descrição :<br><br>
<em>mkdir diretório:</em> Comando para criar um novo diretório, utilizando o terminal do ubuntu.<br><br>
<em>cmake .. :</em> Comando que vai gerar os arquivos CMakeCache.txt, CMakeFiles, cmake_install.cmake e Makefile.<br><br>
<em>make :</em> Comando responsável pela compilação dos arquivos e gera o arquivo executável com o nome citado no CMakeLists.txt (project(test)). <br><br>
<em>./test:</em> Execução do arquivo gerado pelo make.</p><br>



